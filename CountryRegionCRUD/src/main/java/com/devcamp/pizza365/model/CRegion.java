package com.devcamp.pizza365.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
@Entity
@Table(name = "region")
public class CRegion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(unique = true,name = "region_code")
    private String regionCode;
    
    @Column(name = "region_name")
    private String regionName;
    @ManyToOne
    @JsonIgnore
    private CCountry country;
    
    /*
     *  Trong một số trường hợp, trong entity class có chứa một field mà field này không tồn tồn tại trong database. 
     *  Khi đó chúng ta sẽ gặp lỗi “java.sql.SQLSyntaxErrorException: Unknown column ‘additionalPropery’ in ‘field list'”.

        Để tránh lỗi này, chúng ta có thể sử dụng @Transient để thông báo rằng thuộc tính/ phương thức này không liên quan gì tới một cột nào dưới database.
        Khi đó, Hibernate sẽ bỏ qua field này.
     */
    @Transient
    private String countryName;
    @JsonIgnore
    public String getCountryName() {
        return getCountry().getCountryName();
    }
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getRegionName() {
        return regionName;
    }
    
    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getRegionCode() {
        return regionCode;
    }
    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }
    @JsonIgnore
    public CCountry getCountry() {
        return country;
    }

    public void setCountry(CCountry cCountry) {
        this.country = cCountry;
    }
}

